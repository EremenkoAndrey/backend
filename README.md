# README #

Заготовка для верстки приложения с использованием шаблонизатора Twig.js

Скачивание:
```git clone https://bitbucket.org/EremenkoAndrey/backend/```

### Запуск в режиме разработки ###

```npm start```

### Запуск сервера expess в режиме разработки ###

```npm run server```

### Сборка проекта ###

```npm run build```

## Структура проекта ##

- src/
    - components: компоненты react
    - functional: функции (функциональные компоненты) без собственного DOM-представления
    - layouts: основной каркас шаблона
    - modules: функциональные модули
    - nodes: страницы, загружаемые при переходе по роутам
    - reducers: редьюсеры
    - routes: URL маршруты


## Лицензии
В проекте использованы изображения, входящие в пакет 
[Font Awesome Free по лицензии CC BY 4.0 License](https://fontawesome.com/license)


