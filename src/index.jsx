import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createHistory from 'history/createBrowserHistory';
import thunk from 'redux-thunk';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import RootRoutes from 'routes/';
import reducer from 'reducers/';
import 'middleware/'; // Регистрация обработчиков запросов к серверу
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const history = createHistory();
const routersMiddleware = routerMiddleware(history);

const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(
        thunk,
        routersMiddleware
    ))
);


render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <RootRoutes />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
);
