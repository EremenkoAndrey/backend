import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import PageTitle from 'components/PageTitle/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import messages from './lang/';
import './index.less';

const LoginPage = props => (
    <div className="login-page">
        <div className="login-page__form">
            <div className="login-page__form-title">
                <PageTitle>{messages.get('TITLE')}</PageTitle>
            </div>
            <div className="login-page__form-body">
                {props.children}
            </div>
        </div>
    </div>
);

LoginPage.propTypes = {
    children: PropTypes.element
};

LoginPage.defaultProps = {
    children: null
};

export default LoginPage;
