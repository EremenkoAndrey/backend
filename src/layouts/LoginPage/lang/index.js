/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Localization from 'modules/Localization';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import en from './en';
import ru from './ru';

export default new Localization({ en, ru });

