import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Header from 'components/Header/';
import MenuContainer from 'components/Menu/container';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';


const App = props => (
    <div className="app">
        <div className="app__header">
            <Header />
        </div>
        <div className="app__body">
            <aside className="app__sidebar">
                <MenuContainer />
            </aside>
            <div className="app__content">
                {props.children}
            </div>
        </div>
    </div>
);

App.propTypes = {
    children: PropTypes.element
};

App.defaultProps = {
    children: null
};

export default App;
