import React from 'react';
import { Route, Switch } from 'react-router-dom';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'config/';
import Bundle from 'functional/Bundle/';
import PrivateRoute from 'functional/PrivateRoute/';
import Index from 'nodes/Index/';
import Login from 'nodes/Login/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const RootRoutes = () => (
    <Switch>
        <PrivateRoute exact path="/" component={Index} />
        <Route path={Config.get('authPage', '/login')} render={(props) => <Login {...props} />} />
        <PrivateRoute
            exact
            path="/users"
            component={props => (
                <Bundle route={{ ...props }} import={() => import('./users.routes')} />
            )}
        />
    </Switch>
);

export default RootRoutes;
