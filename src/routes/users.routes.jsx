import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Users from 'nodes/Users/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const UsersRoutes = ({ match }) => (
    <Switch>
        <Route exact path={`${match.path}`} render={() => <Users />} />
    </Switch>
);

UsersRoutes.propTypes = {
    match: PropTypes.shape({
        path: PropTypes.string
    }).isRequired
};

export default UsersRoutes;
