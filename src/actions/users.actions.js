export function NEW_USERS(users) {
    const usersObject = users.docs.reduce((acc, nextUser) => {
        acc[nextUser.id] = nextUser;
        return acc;
    }, {});
    return dispatch => dispatch({ type: 'NEW_USERS', payload: usersObject });
}
