/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import API from 'modules/API';
import ERROR from 'actions/errors.actions';
import { NEW_USERS } from 'actions/users.actions';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

export function FETCH_USERS_LIST_SUCCESSFUL(users) {
    return (dispatch) => {
        dispatch(NEW_USERS(users));
        const idsList = users.docs.map(user => user.id);
        return dispatch({ type: 'FETCH_USERS_LIST_SUCCESSFUL', payload: idsList });
    };
}

export function FETCH_USERS_LIST_ERROR(error) {
    return (dispatch) => {
        dispatch(ERROR(error));
        return dispatch({ type: 'FETCH_USERS_LIST_ERROR', payload: error });
    };
}

export function FETCH_USERS_LIST() {
    return (dispatch) => {
        API.get('users')
            .then(
                users => dispatch(FETCH_USERS_LIST_SUCCESSFUL(users)),
                error => dispatch(FETCH_USERS_LIST_ERROR(error))
            );
        dispatch({ type: 'FETCH_USERS_LIST' });
    };
}
