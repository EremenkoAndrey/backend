export function USER_IS_AUTHENTICATED(status) {
    return dispatch => dispatch({
        type: 'USER_IS_AUTHENTICATED',
        payload: { status }
    });
}

export function AUTHORIZATION_SUCCESSFUL() {
    return dispatch => dispatch({ type: 'AUTHORIZATION_SUCCESSFUL' });
}
