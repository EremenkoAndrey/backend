export default function ERROR(error) {
    return (dispatch) => {
        console.log(error);
        return dispatch({ type: 'ERROR', payload: error });
    };
}
