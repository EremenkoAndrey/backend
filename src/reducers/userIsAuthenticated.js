export default (state = false, action) => {
    switch (action.type) {
    case 'USER_IS_AUTHENTICATED':
        return action.payload.status;
    case 'AUTHORIZATION_SUCCESSFUL':
        return true;
    default:
        return state;
    }
};
