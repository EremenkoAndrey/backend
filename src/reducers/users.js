export default (state = {}, action) => {
    switch (action.type) {
    case 'NEW_USERS':
        return Object.assign({}, state, action.payload);
    default:
        return state;
    }
};