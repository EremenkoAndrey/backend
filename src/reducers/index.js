import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import lang from './lang';
import userIsAuthenticated from './userIsAuthenticated';
import users from './users';
import usersList from './usersList';

export default combineReducers({
    lang,
    users,
    userIsAuthenticated,
    usersList,
    router: routerReducer
});
