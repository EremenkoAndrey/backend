/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'config/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

export default (state = Config.get('app.lang')) => state;
