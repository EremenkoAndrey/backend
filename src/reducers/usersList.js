const initialState = {
    items: []
};

export default (state = initialState, action) => {
    switch (action.type) {
    case 'FETCH_USERS_LIST_SUCCESSFUL':
        console.log('FETCH_USERS_LIST_SUCCESSFUL >', action);
        return {
            ...state,
            items: state.items.concat(action.payload)
        };
    default:
        return state;
    }
};
