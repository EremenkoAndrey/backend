import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'config/';
import AuthorizationControl from 'functional/AuthorizationControl/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const PrivateRoute = ({ component: Component, ...rest }) => (<Route
    {...rest}
    render={props => (
        <AuthorizationControl
            allow={() => <Component {...props} />}
            disallow={() => (<Redirect to={{
                pathname: Config.get('authPage', '/login'),
                state: {
                    from: props.location
                }
            }}
            />)}
        />
    )}
/>
);

PrivateRoute.propTypes = {
    component: PropTypes.func.isRequired,
    location: PropTypes.shape({
        pathname: PropTypes.string,
        hash: PropTypes.string,
        search: PropTypes.string
    })
};

PrivateRoute.defaultProps = {
    location: null
};

export default PrivateRoute;
