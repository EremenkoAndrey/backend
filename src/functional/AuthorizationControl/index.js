import { connect } from 'react-redux';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Authorization from 'modules/Authorization';
import { USER_IS_AUTHENTICATED } from 'actions/authorization.actions';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const mapStateToProps = (state, ownProps) => ({
    allow: ownProps.allow,
    disallow: ownProps.disallow
});

const mapDispatchToProps = dispatch => ({
    authorizationChecked: isAuthenticated => dispatch(USER_IS_AUTHENTICATED(isAuthenticated))
});

const AuthorizationControl = ({ authorizationChecked, allow, disallow }) => {
    const userIsAuthenticated = Authorization.check();
    authorizationChecked(userIsAuthenticated);

    return userIsAuthenticated ?
        allow() :
        disallow();
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationControl);
