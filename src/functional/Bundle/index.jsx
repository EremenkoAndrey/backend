import React from 'react';
import PropTypes from 'prop-types';
import Loadable from 'react-loadable';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Loader from 'components/Loader';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const Bundle = (props) => {
    const Component = Loadable({
        loader: props.import,
        loading(state) {
            if (state.error) {
                return (
                    <div style={{ color: 'red' }}>
                    Error {state.error.name}! <br /> {state.error.message}
                    </div>
                );
            } else if (state.pastDelay) {
                return <Loader />;
            }
            return null;
        }
    });
    return <Component {...props.route} />;
};

Bundle.propTypes = {
    import: PropTypes.func.isRequired,
    route: PropTypes.shape({
        history: PropTypes.shape({
            length: PropTypes.number,
            location: PropTypes.shape({
                pathname: PropTypes.string,
                hash: PropTypes.string,
                search: PropTypes.string
            }),
            hash: PropTypes.string,
            push: PropTypes.func,
            replace: PropTypes.func,
            go: PropTypes.func,
            goBack: PropTypes.func,
            goForward: PropTypes.func,
            canGo: PropTypes.func
        }),
        location: PropTypes.shape({
            pathname: PropTypes.string,
            hash: PropTypes.string,
            search: PropTypes.string
        }),
        match: PropTypes.shape({
            url: PropTypes.string,
            match: PropTypes.string,
            isExact: PropTypes.bool
        })
    }).isRequired
};

export default Bundle;
