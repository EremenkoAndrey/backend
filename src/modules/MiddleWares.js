export default class MiddleWares {
    constructor() {
        this._middlewares = {};
    }

    _register({
        method, entryName, req, res
    }) {
        const _method = method.toLowerCase();
        if (!this._middlewares[entryName]) {
            this._middlewares[entryName] = {};
        }
        this._middlewares[entryName][_method] = {
            req,
            res
        };
    }

    /**
     * Найти middleware запроса
     *
     * @param method - (string) Метод (GET, POST...)
     * (под ним зарегистрирован URI в конфиг-файле)
     * @param entryName - (string) required, имя точки входа
     * (под ним зарегистрирован URI в конфиг-файле)
     * @return function|null
     */
    findRequest(method, entryName) {
        const point = this._middlewares[entryName];
        const _method = method.toLowerCase();
        if (point && point[_method] && point[_method].req) {
            return point[_method].req;
        }
        return null;
    }

    /**
     * Найти middleware ответа сервера
     *
     * @param method - (string) Метод (GET, POST...)
     * (под ним зарегистрирован URI в конфиг-файле)
     * @param entryName - (string) required, имя точки входа
     * (под ним зарегистрирован URI в конфиг-файле)
     * @return function|null
     */
    findResponse(method, entryName) {
        const point = this._middlewares[entryName];
        const _method = method.toLowerCase();
        if (point && point[_method] && point[_method].res) {
            return point[_method].res;
        }
        return null;
    }

    /**
     * Зарегистрировать middleware для GET-запроса
     *
     * @param entryName - (string) required, имя точки входа
     * (под ним зарегистрирован URI в конфиг-файле)
     * @param req - function
     * @param res - function
     * @returns Promise
     */
    get(entryName, req, res) {
        this._register({
            method: 'get',
            entryName,
            req,
            res
        });
    }
    post(entryName, req, res) {
        this._register({
            method: 'post',
            entryName,
            req,
            res
        });
    }
    put(entryName, req, res) {
        this._register({
            method: 'put',
            entryName,
            req,
            res
        });
    }
    del(entryName, req, res) {
        this._register({
            method: 'del',
            entryName,
            req,
            res
        });
    }
}
