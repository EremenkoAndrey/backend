import Cookies from 'js-cookie';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import API from 'modules/API';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */


export default class Authorization {
    static get cookieName() {
        return '_rtb';
    }
    static get token() {
        if (!this._token) {
            this._token = '';
        }
        return this._token;
    }
    static set token(token) {
        this._token = token;
    }
    static check() {
        const _cookieValue = Cookies.get(this.cookieName);
        if (_cookieValue) {
            this.token = _cookieValue;
            return true;
        }
        return false;
    }

    static setCookies(value, keep) {
        Cookies.set(this.cookieName, value, {
            expires: keep ? 365 : undefined
        });
    }

    static try(login, password, keep = false) {
        return new Promise((resolve, reject) => {
            API.post('auth', { login, password })
                .then(
                    (res) => {
                        if (res.error) {
                            reject(res.error);
                        }
                        if (!res.token) {
                            return reject(new Error('Token not received'));
                        }
                        this.token = res.token;
                        this.setCookies(res.token, keep);
                        return resolve();
                    },
                    err => reject(err)
                );
        });
    }
}
