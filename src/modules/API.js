/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import ServerConnect from 'modules/ServerConnect';
import MiddleWares from 'modules/MiddleWares';
import Config from 'config/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */


/**
 * Отправка:
 *
 * API.post('auth')
 *
 * *********************
 * Добавление middlewear:
 *
 * const use = API.use();
 * use.get('auth', req => req, res => res)
 *
 */

export default class API {
    static get hostUrl() {
        if (!this._hostUrl) {
            this._hostUrl = `${Config.get('app.protocol')}://${Config.get('app.host')}`;
        }
        return this._hostUrl;
    }
    static get serverConnect() {
        if (!this._serverConnect) {
            this._serverConnect = ServerConnect;
        }
        return this._serverConnect;
    }
    static get middleWares() {
        if (!this._middleWares) {
            this._middleWares = new MiddleWares();
        }
        return this._middleWares;
    }

    /**
     * Проксирование GET-запроса на сервер
     *
     * @param entryName - (string) required, имя точки входа
     * (под ним зарегистрирован URI в конфиг-файле)
     * @param params - (string|object) парметры,
     * которые будут переданы на сервер
     * @returns Promise
     */
    static get(entryName, params) {
        const fullPath = this._getFullPath(entryName);
        const { requestMiddleware, responseMiddleware } = this._getMiddleWares('get', entryName);
        return this.serverConnect.get(fullPath, params, requestMiddleware, responseMiddleware);
    }
    static post(entryName, params) {
        const fullPath = this._getFullPath(entryName);
        const { requestMiddleware, responseMiddleware } = this._getMiddleWares('post', entryName);
        return this.serverConnect.post(fullPath, params, requestMiddleware, responseMiddleware);
    }
    static put() {

    }
    static del() {

    }
    static use() {
        return this.middleWares;
    }
    static _getFullPath(entryName) {
        const endPoint = Config.get(`api.${entryName}`, null);
        if (endPoint === null && process.env.NODE_ENV !== 'production') {
            throw new Error(`API entry point ${entryName} does not exist in the config file`);
        }
        return `${this.hostUrl}/${endPoint}`;
    }
    static _getMiddleWares(methodName, entryName) {
        const requestMiddleware = this.middleWares.findRequest(methodName, entryName);
        const responseMiddleware = this.middleWares.findResponse(methodName, entryName);
        return { requestMiddleware, responseMiddleware };
    }
}
