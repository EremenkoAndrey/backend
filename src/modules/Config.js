export default class Config {
    static get config() {
        if (!this._config) {
            this._config = {};
        }
        return this._config;
    }

    static add(props) {
        if (!this._config) {
            this._config = {};
        }

        Object.keys(props).forEach((propName) => {
            if (typeof this._config[propName] === 'undefined') {
                this._config[propName] = props[propName];
            } else {
                this._config[propName] = Object.assign({}, this._config[propName], props[propName]);
            }
        });
    }
    /**
     * Принимает путь к полю в конфигурации в точечной нотации
     * возвращает значения поля, если оно есть или пустую строку,
     * вторым аргументом принимает значение по умолчанию
     *
     * Пример вызова:
     *     Config.get('app.siteName', 'Безымянный сайт')
     *
     * @param fieldName - (string) required
     * @param defaultValue - (string)
     * @returns string
     */
    static get(fieldName, defaultValue = '') {
        const stringToArray = fieldName.split('.');
        // Если в массиве одно значение, значит пришла строка -
        // возвращаем поле в конфиге или дефолтное значение
        if (stringToArray.length === 1) {
            return typeof this.config[fieldName] !== 'undefined' ? this.config[fieldName] : defaultValue;
        }

        const searchResult = stringToArray.reduce((val, field, index) => {
            if (index === 1) {
                return typeof this.config[val] === 'object' ? this.config[val][field] : this.config[val];
            }
            switch (typeof val) {
            case ('object'):
                return val[field];
            case ('undefined'):
                return val;
            default:
                return field;
            }
        });
        return typeof searchResult !== 'undefined' ? searchResult : defaultValue;
    }
}
