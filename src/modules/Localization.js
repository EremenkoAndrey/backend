/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'config/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */


export default class Localization {
    static get defaultLang() {
        if (!Localization._defaultLang) {
            Localization._defaultLang = Config.get('app.lang');
        }
        return Localization._defaultLang;
    }

    constructor(langs = {}) {
        this._langs = langs;
    }

    /**
     * Сначала ищет фразы из переданного языка (если он указан первым
     * аргументом), затем фразы языка, указанного как язык по умолчанию
     * в конфиге, если не фразы не найдены и указано дефолтное значение,
     * то вернет его. Если дефолтное значение не указано, вернет объект с фразами.
     *
     * @param phrase - (string|null) required
     * @param lang - string
     * @param defaultValue - any
     * @returns {*}
     */
    get(phrase, lang, defaultValue = '') {
        let langObject;
        let isNotDefaultLang = false;

        if (lang && typeof this._langs[lang] !== 'undefined') { // Если язык передан
            langObject = this._langs[lang];
            isNotDefaultLang = true;
        } else if (typeof this._langs[Localization.defaultLang] !== 'undefined') { // Ищем в языке по умолчанию
            langObject = this._langs[Localization.defaultLang];
        } else {
            langObject = this._langs; // Если языка нет - ищем фразу в объекте
        }

        // фраза не передана - вернуть найденый объект
        if (!phrase) {
            return langObject;
        }
        // Фраза найдена в объекте - вернуть фаразу
        if (typeof langObject[phrase] !== 'undefined') {
            return langObject[phrase];
        }
        // Если фраза не найдена в объекте
        // и она искалась не в дефолтном языке - поискать в дефолтном
        if (isNotDefaultLang && typeof this._langs[Localization.defaultLang] !== 'undefined') {
            return typeof this._langs[Localization.defaultLang][phrase] !== 'undefined' ?
                this._langs[Localization.defaultLang][phrase] :
                defaultValue;
        }
        // Если ничего не подходит - вернуть дефолтное значение
        return defaultValue;
    }
}
