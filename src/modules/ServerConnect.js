export default class ServerConnect {
    /**
     * GET-запрос на сервер
     *
     * @param entryPoint - (string) required
     * @param params - (string|object)
     * @param requestMiddleware - function
     * @param responseMiddleware - function
     * @returns Promise
     */
    static get(entryPoint, params = '', requestMiddleware = null, responseMiddleware = null) {
        // Если нужна предобработка запроса функцией requestMiddleware
        const _params = (requestMiddleware) ? requestMiddleware(params) : params;
        let _url;
        // Формируем GET строку из параметров
        if (typeof _params === 'string') {
            _url = _params.length ? `${entryPoint}/${params}` : entryPoint;
        } else {
            _url = Object.keys(_params).reduce((acc, key, index) => {
                const separator = (index > 0) ? '&' : '';
                return `${acc}${separator}${key}=${_params[key]}`;
            }, `${entryPoint}?`);
        }

        return new Promise((resolve, reject) => {
            fetch(_url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json(res))
                .then((json) => {
                    const _json = responseMiddleware ?
                        responseMiddleware(null, json, params) :
                        json;
                    return resolve(_json);
                })
                .catch((err) => {
                    const _err = responseMiddleware ? responseMiddleware(err) : err;
                    return reject(_err);
                });
        });
    }

    /**
     * POST-запрос на сервер
     *
     * @param entryPoint - (string) required
     * @param params - (string|object)
     * @param requestMiddleware - function
     * @param responseMiddleware - function,
     * @param method - (string)
     * @returns Promise
     */
    static post(entryPoint, params = '', requestMiddleware = null, responseMiddleware = null, method = 'POST') {
        // Если нужна предобработка запроса функцией requestMiddleware
        const _params = (requestMiddleware) ? requestMiddleware(params) : params;
        const body = (typeof _params === 'string') ? _params : JSON.stringify(_params);

        return new Promise((resolve, reject) => {
            fetch(entryPoint, {
                method,
                headers: {
                    'Content-Type': 'application/json'
                },
                body
            })
                .then((res) => {
                    if (!res.ok) {
                        const error = {
                            status: res.status,
                            message: res.statusText
                        };
                        return reject(responseMiddleware ? responseMiddleware(error) : error);
                    }
                    return res.json(res);
                })
                .then(json => (
                    resolve(responseMiddleware ? responseMiddleware(null, json, params) : json)
                ))
                .catch(err => reject(responseMiddleware ? responseMiddleware(err) : err));
        });
    }
    put(entryPoint, params = '', requestMiddleware = null, responseMiddleware = null) {
        return this.post(entryPoint, params, requestMiddleware, responseMiddleware, 'PUT');
    }
    del(entryPoint, params = '', requestMiddleware = null, responseMiddleware = null) {
        return this.post(entryPoint, params, requestMiddleware, responseMiddleware, 'DELETE');
    }
}
