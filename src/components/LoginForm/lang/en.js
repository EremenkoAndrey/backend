export default {
    DESCRIPTION: 'Sign in to start your session',
    LOG_IN: 'Login',
    REMEMBER_ME: 'Remember me',
    INCORRECT_AUTH_DATA: 'Incorrect e-mail or password',
    SERVER_AUTH_ERROR: 'Authorization server error'
};
