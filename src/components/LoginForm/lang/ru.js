export default {
    DESCRIPTION: 'Войдите, чтобы начать сеанс',
    LOG_IN: 'Авторизация',
    REMEMBER_ME: 'Запомнить',
    INCORRECT_AUTH_DATA: 'Пара e-mail/пароль не найдена ',
    SERVER_AUTH_ERROR: 'Ошибка сервера авторизации'
};
