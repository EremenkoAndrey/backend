import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Input from 'components/Input/';
import Checkbox from 'components/Checkbox/';
import Button from 'components/Button/';
import ErrorText from 'components/ErrorText/';
import Authorization from 'modules/Authorization';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import messages from './lang/';
import './index.less';

export default class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            email: '',
            remember: true,
            error: ''
        };
    }

    handleChange = ({ name, value }) => {
        this.setState({
            [name]: value
        });
    };

    tryAuth = () => {
        const { email, password, remember } = this.state;
        Authorization.try(email, password, remember)
            .then(
                () => this.props.authSuccessful(),
                (err) => {
                    this.setState({
                        error: (err.status === 401) ?
                            messages.get('INCORRECT_AUTH_DATA') :
                            messages.get('SERVER_AUTH_ERROR')
                    });
                }
            );
    };

    render() {
        return (
            <div className="login-form">
                {this.props.message ?
                    <div className="login-form__msg">
                        {this.props.message}
                    </div>
                    : null
                }

                <ErrorText>
                    <div className="login-form__error">{this.state.error}</div>
                </ErrorText>

                <div className="login-form__field login-form__field_email">
                    <Input
                        value={this.state.email}
                        onChange={this.handleChange}
                        name="email"
                        type="email"
                        placeholder="E-mail"
                    />
                </div>
                <div className="login-form__field login-form__field_password">
                    <Input
                        value={this.state.password}
                        onChange={this.handleChange}
                        name="password"
                        type="password"
                        placeholder="Password"
                    />
                </div>
                <div className="login-form__actions">
                    <div className="login-form__remember">
                        <Checkbox
                            checked={this.state.remember}
                            onChange={() => {}}
                            text={this.props.checkboxTxt}
                        />
                    </div>
                    <div className="login-form__button">
                        <Button
                            mods={['blue']}
                            text={this.props.buttonTxt}
                            onClick={this.tryAuth}
                        />
                    </div>
                </div>
            </div>
        );
    }
}
LoginForm.propTypes = {
    authSuccessful: PropTypes.func.isRequired,
    message: PropTypes.string,
    buttonTxt: PropTypes.string,
    checkboxTxt: PropTypes.string
};

LoginForm.defaultProps = {
    message: messages.get('DESCRIPTION'),
    buttonTxt: messages.get('LOG_IN'),
    checkboxTxt: messages.get('REMEMBER_ME')
};

