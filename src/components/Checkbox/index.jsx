import React from 'react';
import PropTypes from 'prop-types';
import './index.less';

const Checkbox = ({
    text, name, mods, elem, checked, onChange
}) => {
    const classes = {
        label: `checkbox
                ${elem || ''}
                ${mods.length ? ` checkbox_${mods.join(' checkbox_')}` : ''}
                ${checked ? 'checkbox_checked' : ''}`,
        input: `checkbox__input
                ${mods.length ? ` checkbox__input_${mods.join(' checkbox__input_')}` : ''}
                ${text ? ' checkbox__input_text' : ''}`,
        text: `checkbox__text ${mods.length ? ` checkbox__text_${mods.join(' checkbox__text_')}` : ''}`
    };

    const handleChange = (e) => {
        onChange({
            checked: e.target.checked,
            text,
            name
        });
    };

    return (
        <label className={classes.label}>
            <input
                type="checkbox"
                name={name}
                className={classes.input}
                checked={checked}
                onChange={handleChange}
            />
            {text ? <span className={classes.text}>{text}</span> : ''}
        </label>);
};

Checkbox.defaultProps = {
    mods: [],
    elem: '',
    text: '',
    name: 'checkbox'
};

Checkbox.propTypes = {
    // Состояние по умолчанию
    checked: PropTypes.bool.isRequired,
    // Обработчик изменения состояния из родительского компонента
    onChange: PropTypes.func.isRequired,
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-класс элемента
    elem: PropTypes.string,
    // Текст описания чекбокса
    text: PropTypes.string,
    // Значение поля name чекбокса
    name: PropTypes.string
};

export default Checkbox;
