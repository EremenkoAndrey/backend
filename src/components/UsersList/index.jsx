import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import 'components/Table';
import UserListItem from 'components/UserListItem/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import messages from './lang/';
import './index.less';

const UsersList = props => (
    <div className="users-list table">
        <div className="users-list__head table__row">
            {props.columns.map(column => (
                <div key={column.code} className="users-list__cell table__cell">{column.title}</div>
            ))}
        </div>
        {props.usersList.map(id => <UserListItem key={id} userId={id} />)}
    </div>
);

UsersList.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string,
        code: PropTypes.string
    })),
    usersList: PropTypes.arrayOf(PropTypes.string)
};

UsersList.defaultProps = {
    usersList: [],
    columns: [
        {
            code: 'name',
            title: messages.get('NAME')
        },
        {
            code: 'email',
            title: messages.get('EMAIL')
        },
        {
            code: 'role',
            title: messages.get('ROLE')
        },
        {
            code: 'status',
            title: messages.get('STATUS')
        }
    ]
};

const mapStateToProps = state => ({
    usersList: state.usersList.items
});

export default connect(mapStateToProps)(UsersList);
