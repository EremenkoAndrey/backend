import React from 'react';
import PropTypes from 'prop-types';
import './index.less';


const ErrorText = props => (
    props.children ?
        <span className="error-text">{props.children}</span> :
        null
);

ErrorText.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ])
};

ErrorText.defaultProps = {
    children: ''
};

export default ErrorText;
