import React from 'react';
import PropTypes from 'prop-types';
import './index.less';

const PageTitle = (props) => {
    if (props.children) {
        return (
            <h1
                className={`page-title
                ${props.mods.length ? ` page-title_${props.mods.join(' page-title_')}` : ''}
                ${props.elem ? props.elem : ''}`}
            >
                {props.children}
            </h1>
        );
    }
    return null;
};

PageTitle.propTypes = {
    children: PropTypes.string,
    mods: PropTypes.arrayOf(PropTypes.string),
    elem: PropTypes.string
};

PageTitle.defaultProps = {
    children: '',
    mods: [],
    elem: ''
};

export default PageTitle;
