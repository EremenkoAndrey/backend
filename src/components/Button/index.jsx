import React from 'react';
import PropTypes from 'prop-types';
import './index.less';

const Button = props => (
    <button
        className={`button
            ${props.elem ? props.elem : ''}
            ${props.mods.length ? ` button_${props.mods.join(' button_')}` : ''}`}
        onClick={props.onClick}
    >
        {props.text}
    </button>
);

Button.propTypes = {
    // Текст кнопки
    text: PropTypes.string,
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-элемент
    elem: PropTypes.string,
    // Обработчик клика
    onClick: PropTypes.func
};

Button.defaultProps = {
    text: '',
    mods: [],
    elem: '',
    onClick: null
};

export default Button;
