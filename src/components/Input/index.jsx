import React from 'react';
import PropTypes from 'prop-types';
import './index.less';

const Input = (props) => {
    const handleChange = (event) => {
        props.onChange({
            value: event.target.value,
            name: event.target.name
        });
    };

    const classes = `input input_type_${props.type}
            ${props.elem ? props.elem : ''}
            ${props.mods.length ? ` input_${props.mods.join(' input_')}` : ''}`;

    return (
        <input
            type={props.type}
            name={props.name}
            placeholder={props.placeholder}
            className={classes}
            onChange={handleChange}
            value={props.value}
            step={props.step}
            autoComplete={props.autocomplete}
            min={props.min}
            max={props.max}
        />
    );
};

Input.defaultProps = {
    value: '',
    placeholder: '',
    mods: [],
    elem: '',
    name: 'text',
    type: 'text',
    step: null,
    min: null,
    max: null,
    autocomplete: 'on'
};

Input.propTypes = {
    // Поле value
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    // Обработчик изменения состояния из родительского компонента
    onChange: PropTypes.func.isRequired,
    // Плейсхолдер
    placeholder: PropTypes.string,
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-элемент
    elem: PropTypes.string,
    // Значение поля name
    name: PropTypes.string,
    // Значение поля type
    type: PropTypes.string,
    // Указать шаг для полей типа number
    step: PropTypes.string,
    // Минимум/максимум для полей типа number
    min: PropTypes.string,
    max: PropTypes.string,
    // Автозаполнение
    autocomplete: PropTypes.string
};

export default Input;
