import { connect } from 'react-redux';
import Menu from './index';

const mapStateToProps = state => ({
    lang: state.lang
});


const MenuContainer = connect(mapStateToProps)(Menu);

export default MenuContainer;
