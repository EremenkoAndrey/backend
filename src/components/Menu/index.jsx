import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import messages from './lang/';
import './index.less';

const Menu = (props) => {
    const items = [
        {
            href: '/users',
            name: messages.get('NAME', props.lang),
            mod: 'users'
        },
        {
            href: '/login',
            name: 'Login'
        }
    ];

    return (
        <nav className={`menu ${props.elem ? props.elem : ''}`}>
            {items.map(item => (
                <NavLink className="menu__link" to={item.href} key={item.name}>
                    {item.name}
                </NavLink>
            ))}
        </nav>
    );
};

Menu.propTypes = {
    lang: PropTypes.string.isRequired,
    elem: PropTypes.string
};

Menu.defaultProps = {
    elem: ''
};

export default Menu;
