import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'config/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const Logo = props => NavLink({
    to: '/',
    className: `logo
        ${props.mods.length ? ` logo_${props.mods.join(' logo_')}` : ''}
        ${props.elem ? props.elem : ''}`,
    title: props.title || '',
    children: props.title || ''
});

Logo.propTypes = {
    title: PropTypes.string,
    mods: PropTypes.arrayOf(PropTypes.string),
    elem: PropTypes.string
};

Logo.defaultProps = {
    title: Config.get('app.siteName', 'No name'),
    mods: [],
    elem: ''
};

export default Logo;
