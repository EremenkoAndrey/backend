import React from 'react';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Logo from 'components/Logo';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const Header = () => (
    <header className="header">
        <Logo elem="header__logo" />
    </header>
);

export default Header;
