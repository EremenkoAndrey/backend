import React from 'react';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import 'components/Content';
import App from 'layouts/App/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const Index = () => (
    <App>
        <div className="content content_index">Index page is created!</div>
    </App>
);

export default Index;
