import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import 'components/Content';
import App from 'layouts/App/';
import Config from 'config/';
import PageTitle from 'components/PageTitle/';
import UsersList from 'components/UsersList/';
import { FETCH_USERS_LIST } from 'actions/usersList.actions';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import messages from './lang/';


class Users extends React.Component {
    componentDidMount() {
        this.props.fetchUsersList();
    }

    render() {
        return (
            <App>
                <section className="content content_users">
                    <div className="content__title">
                        <PageTitle>{messages.get('TITLE', this.props.lang)}</PageTitle>
                    </div>
                    <div className="content__body">
                        <UsersList />
                    </div>
                </section>
            </App>
        );
    }
}

Users.propTypes = {
    lang: PropTypes.string,
    fetchUsersList: PropTypes.func
};

Users.defaultProps = {
    lang: Config.get('lang'),
    fetchUsersList: null
};

const mapDispatchToProps = dispatch => ({
    fetchUsersList: () => {
        dispatch(FETCH_USERS_LIST());
    }
});

const mapStateToProps = state => ({
    lang: state.lang
});

export default connect(mapStateToProps, mapDispatchToProps)(Users);
