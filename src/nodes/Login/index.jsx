import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import isset from 'utils/isset';
import Authorization from 'modules/Authorization';
import LoginPage from 'layouts/LoginPage/';
import LoginForm from 'components/LoginForm/';
import { USER_IS_AUTHENTICATED, AUTHORIZATION_SUCCESSFUL } from 'actions/authorization.actions';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const Login = ({
    authCheckResult,
    userIsAuthenticated,
    location,
    authSuccessful
}) => {
    authCheckResult(Authorization.check());
    return (
        (userIsAuthenticated) ?
            <Redirect to={{
                pathname: isset(location, 'location.state.from.pathname') ? location.state.from.pathname : '/',
                state: {
                    from: location
                }
            }}
            /> :
            <LoginPage>
                <LoginForm authSuccessful={authSuccessful} />
            </LoginPage>
    );
};


Login.propTypes = {
    // Текущий статус авторизации пользователя
    userIsAuthenticated: PropTypes.bool.isRequired,
    // В эту функцию будет передан результат проверки авторизации
    authCheckResult: PropTypes.func,
    // Функция, которая вызывается при успешной авторизации
    authSuccessful: PropTypes.func,
    // Объект роутинга
    location: PropTypes.shape({
        state: PropTypes.shape({
            from: PropTypes.shape({
                pathname: PropTypes.string
            })
        })
    }).isRequired
};

Login.defaultProps = {
    authSuccessful: null,
    authCheckResult: null
};

const mapStateToProps = state => ({
    userIsAuthenticated: state.userIsAuthenticated
});

const mapDispatchToProps = dispatch => ({
    authSuccessful: () => dispatch(AUTHORIZATION_SUCCESSFUL()),
    authCheckResult: status => dispatch(USER_IS_AUTHENTICATED(status))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);
