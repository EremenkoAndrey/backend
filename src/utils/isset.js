/**
Принимает объект и строковое представление вложенного в объект свойства
Примеры:
const obj = {test1:{test5: true}};
isset(obj, 'obj.test1.test5'); // true

const obj = {test1:[{test5: true}]};
isset(obj, 'obj.test1[0].test5'); // true
isset(obj, 'obj.test1.test5'); // false
return bool

Если не передавать второй аргумент, то проверяет
переданное значение на undefined
 */
export default function isset(obj, props, index = 1) {
    if (typeof obj === 'undefined') return false;

    let arrProps;
    if (typeof props === 'undefined') {
        return typeof obj === 'undefined';
    } else if (typeof props === 'string') {
        const reg = /]|\[|\./;
        arrProps = props.split(reg).filter(el => el !== '');
    } else {
        arrProps = props;
    }

    if (Array.isArray(obj)) {
        const arrIndex = parseInt(arrProps[index], 10);
        const result = !Number.isNaN(arrIndex) && typeof obj[arrIndex] !== 'undefined';

        if (result && arrProps[index + 1]) {
            return isset(obj[arrProps[index]], props, index + 1);
        }
        return result;
    }

    const hasOwnProperty = Object.prototype.hasOwnProperty.call(obj, arrProps[index]);
    if (hasOwnProperty && arrProps[index + 1]) {
        return isset(obj[arrProps[index]], props, index + 1);
    }
    return hasOwnProperty;
}
