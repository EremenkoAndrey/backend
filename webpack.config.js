const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const merge = require('webpack-merge');
const devServer = require('./config/builder/devServer.config.js');
const devConfig = require('./config/builder/webpack.dev.config.js');
const prodConfig = require('./config/builder/webpack.prod.config.js');

const config = process.env.NODE_ENV !== 'production' ? devConfig : prodConfig;

module.exports = merge(config, {
    entry: {
        index: './src/index.jsx'
    },
    output: {
        filename: '[name].js',
        path: `${__dirname}/dist`
    },
    resolve: {
        extensions: ['.js', '.jsx', '.less'],
        alias: {
            config: path.resolve(__dirname, 'config'),
            components: path.resolve(__dirname, 'src/components'),
            layouts: path.resolve(__dirname, 'src/layouts'),
            nodes: path.resolve(__dirname, 'src/nodes'),
            routes: path.resolve(__dirname, 'src/routes'),
            functional: path.resolve(__dirname, 'src/functional'),
            reducers: path.resolve(__dirname, 'src/reducers'),
            actions: path.resolve(__dirname, 'src/actions'),
            modules: path.resolve(__dirname, 'src/modules'),
            middleware: path.resolve(__dirname, 'src/middleware'),
            utils: path.resolve(__dirname, 'src/utils')
        }
    },
    plugins: [
        new CleanWebpackPlugin(['dist/']),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/index.twig'
        }),
        new ExtractTextPlugin('styles/styles.css')
    ],
    devServer,
    module: {
        rules: [
            {
                test: /\.js$|.jsx$/,
                use: [
                    'babel-loader'
                ],
                exclude: /node_modules/
            },
            {
                test: /\.twig$/,
                use: [
                    'twig-loader'
                ],
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.less$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader'
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                config: {
                                    path: './config/builder/postcss.config.js'
                                }
                            }
                        },
                        {
                            loader: 'less-loader'
                        }]
                })
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            context: 'src/'
                        }
                    }
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            context: 'src/'
                        }
                    }
                ]
            }
        ]
    }
});

