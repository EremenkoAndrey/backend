import { expect } from 'chai';
import Localization from './../src/modules/Localization/';
import Config from './../config/';


const mockLangs = {
    en: {
        TEXT: '_TEXT_',
        ANOTHER_TEXT: '_ANOTHER_TEXT_',
        ARRAY: [{
            NAME: 'users',
            HREF: '/users'
        }]
    },
    ru: {
        TEXT: 'РУССКИЙ ТЕКСТ',
        ARRAY: [{
            NAME: 'Пользователи',
            HREF: '/users'
        }]
    }
};

const defaultLang = Config.get('app.lang');

if (defaultLang !== 'en') {
    throw new Error('Для тестирования необходимо установить язык по умолчанию en');
}

const messages = new Localization({
    en: mockLangs.en,
    ru: mockLangs.ru
});

describe('Localization method get()', () => {
    it('Без параметров возвращает объект языка, установленного по-умолчанию', () => {
        expect(messages.get()).to.deep.equal(mockLangs[defaultLang]);
    });
    it('Если передать фразу, вернет ее', () => {
        expect(messages.get('TEXT')).to.be.equal('_TEXT_');
    });
    it('Если передать язык и фразу, вернет фразу на этом языке', () => {
        expect(messages.get('TEXT', 'ru')).to.be.equal('РУССКИЙ ТЕКСТ');
    });
    it('Неправильная фраза, правильный язык - дефолтное значение', () => {
        expect(messages.get('TEXT_FAKE', 'ru', 'DEFAULT')).to.be.equal('DEFAULT');
    });
    it('Правильная фраза, неправильный язык - фраза из языка по умолчанию', () => {
        expect(messages.get('TEXT', 'fake_lang', 'DEFAULT')).to.be.equal('_TEXT_');
    });
    it('Фразы нет в переданном языке, но есть в языке по умолчанию - фраза из языка по умолчанию', () => {
        expect(messages.get('ANOTHER_TEXT', 'ru', 'DEFAULT')).to.be.equal('_ANOTHER_TEXT_');
    });
    it('Может вернуть сложную структуру', () => {
        expect(messages.get('ARRAY')).to.deep.equal(mockLangs[defaultLang].ARRAY);
    });
    it('Если ничего не найдено - вернет пустую строку', () => {
        expect(messages.get('TEXT_FAKE', 'fake_lang')).to.be.equal('');
    });
    it('Если не педелать поле для поиска, то вернет оъект языка ', () => {
        expect(messages.get(null, 'ru')).to.deep.equal(mockLangs.ru);
    });
});

