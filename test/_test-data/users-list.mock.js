const dummyUsersList = [
    {
        id: 'user-0',
        fields: {
            name: {
                value: 'Ivan Katkov',
                edit: true,
                delete: false,
                show: true
            },
            email: {
                value: 'ivan@webspec.ru',
                edit: true,
                delete: false,
                show: true
            },
            role: {
                value: 'Owner',
                edit: true,
                delete: false,
                show: true
            },
            status: {
                value: 'Active',
                edit: true,
                delete: false,
                show: true
            }
        },
        edit: true,
        delete: false,
        show: true
    },
    {
        id: 'user-1',
        fields: {
            name: {
                value: 'Sergey Kukota',
                edit: true,
                delete: false,
                show: true
            },
            email: {
                value: 'smkukota@rttv.ru',
                edit: true,
                delete: false,
                show: true
            },
            role: {
                value: 'Admin',
                edit: true,
                delete: false,
                show: true
            },
            status: {
                value: 'Active',
                edit: true,
                delete: false,
                show: true
            }
        },
        edit: true,
        delete: false,
        show: true
    },
    {
        id: 'user-2',
        fields: {
            name: {
                value: 'Elvira Chudnovskaya',
                edit: true,
                delete: false,
                show: true
            },
            email: {
                value: 'elvirachudnovskaya@gmail.com',
                edit: true,
                delete: false,
                show: true
            },
            role: {
                value: 'Admin',
                edit: true,
                delete: false,
                show: true
            },
            status: {
                value: 'Active',
                edit: true,
                delete: false,
                show: true
            }
        },
        edit: true,
        delete: false,
        show: true
    },
    {
        id: 'user-3',
        fields: {
            name: {
                value: 'Vera Serzhantova',
                edit: true,
                delete: false,
                show: true
            },
            email: {
                value: 'vsserzhantova@rttv.ru',
                edit: true,
                delete: false,
                show: true
            },
            role: {
                value: 'Admin',
                edit: true,
                delete: false,
                show: true
            },
            status: {
                value: 'Active',
                edit: true,
                delete: false,
                show: true
            }
        },
        edit: true,
        delete: false,
        show: true
    },
    {
        id: 'user-4',
        fields: {
            name: {
                value: 'Alexandra Rumyantseva',
                edit: true,
                delete: false,
                show: true
            },
            email: {
                value: 'aarumyantseva@rttv.ru',
                edit: true,
                delete: false,
                show: true
            },
            role: {
                value: 'Editor',
                edit: true,
                delete: false,
                show: true
            },
            status: {
                value: 'Active',
                edit: true,
                delete: false,
                show: true
            }
        },
        edit: true,
        delete: false,
        show: true
    }
];

module.exports = dummyUsersList;
