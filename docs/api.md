## Работа с API

### Создание endpoint:

В конфиге, в секцию config.api добавляется поле.

```javascript
{
    auth: process.env.NODE_ENV !== 'development' ? '/auth' : 'api/auth' 
}
```
Можно зарегистрировать различные маршруты в зависимости 
от переменной окружения.

###Обращения к серверу

Обращения к серверу из компонентов реализуются таким образом:

```javascript
import API from 'modules/API';

API.get('auth', { id: 5 })
    .then(res => console.log('response ', res));
```

В результате будет произведен поиск зарегистрированного 
под ключем 'auth' маршрута в секции 'api' конфиг-файла.

Вторым аргументом в фугкцию `API.get` передается объект или строка с параметрами.

## Обработчики запроса и ответа сервера

Все данные передаются на сервер из компонента "как есть", то есть 
как обычный JS-объект или массив. Так же от сервера компонент 
ожидает данные в определенном формате. Чтобы не связывать особенности 
конкретной реализации API со структурой данных в компонентах реализована система
промежуточных обработчиков (middleware).

Все обработчики расположены в директории `middleware/`.

### Добавление обработчика

Обработчик регистрируется в отдельном файле в директории `middleware/`,
а затем этот файл импортируется в файл `middleware/index.js`

Пример регистрации обработчика для GET-запроса для маршрута,
зарегистрированного под именем 'auth':
```javascript
import API from 'modules/API';
const use = API.use();

use.get(
    'auth',
    req => req,
    (err, res, req) => {
        res.test = true;
        return res;
    }
);
```
Функция `API.use().get()` регистрирует обработчик для GET-запроса,
первым аргументом она принимает имя точки входа, вторым - функцию, 
в которую будет передан объект параметров запроса, здесь из можно,
например преобразовать в JSON, добавить или удалить что-то.
Третий аргумент - функция, в которую будет переданны данные, 
полученные от сервера. Если получена ошибка, она будет доступна 
в первом аргументе функции, если запрос успешен, ответ придет 
во втором аргументе, в третьем аргументе будет объект запроса 
до преобразования.
ЕСЛИ ПЕРЕДАНА ФУНКЦИЯ, ОНА ДОЛЖНА ВЕРНУТЬ ПРЕОБРАЗОВАННЫЙ ОБЪЕКТ!
