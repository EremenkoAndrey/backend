/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'modules/Config';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import app from './app';
import api from './api';

Config.add({
    app,
    api
});

export default Config;
