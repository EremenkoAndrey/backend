const api = {
    auth: process.env.NODE_ENV !== 'development' ? 'api/auth' : '',
    users: process.env.NODE_ENV !== 'development' ? 'api/users' : ''
};

export default api;
