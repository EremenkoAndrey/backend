const app = {
    // Название сайта
    siteName: 'KYБ',

    // Язык по умолчанию
    lang: 'en',

    // Страница авторизации
    authPage: '/login',

    // Домен сайта
    host: 'localhost:8080',

    // Протокол
    protocol: 'http'
};

export default app;
