const bodyParser = require('body-parser');
const dummyUsersList = require('./../../test/_test-data/users-list.mock');

module.exports = {
    historyApiFallback: {
        rewrites: [
            { from: /\/api\//, to: context => context.parsedUrl.pathname },
            { from: /./, to: '/' }
        ]
    },
    setup(app) {
        app.use(bodyParser.json());

        app.post('/api/auth', bodyParser.json(), (request, response) => {
            if (request.body.login === 'admin@mail.ru' && request.body.password === '123456') {
                console.log('Успешная авторизация');
                return response.json({
                    code: 200,
                    token: 'TEST_TOKEN'
                });
            }
            if (request.body.password === 'test-error') {
                console.log('Эмуляция ошибки сервера');
                return response.status(500).json({ message: 'Server error' });
            }
            console.log('Эмуляция ошибки авторизации');
            return response.status(401).json({ message: 'Incorrect login or pass' });
        });

        app.get('/api/users', (request, response) => {
            console.log('Users request');
            return response.status(200).json({
                docs: dummyUsersList
            });
        });
    }
};
